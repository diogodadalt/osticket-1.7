<?php
/*************************************************************************
    tickets.php

    Handles all tickets related actions.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

require('staff.inc.php');
require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.dept.php');
require_once(INCLUDE_DIR.'class.filter.php');
require_once(INCLUDE_DIR.'class.canned.php');


$page='';
$ticket=null; //clean start.
//LOCKDOWN...See if the id provided is actually valid and if the user has access.
if($_REQUEST['id']) {
    if(!($ticket=Ticket::lookup($_REQUEST['id'])))
         $errors['err']='Identificação de tíquete desconhecida ou inválida.';
    elseif(!$ticket->checkStaffAccess($thisstaff)) {
        $errors['err']='Acesso negado. Contate a administração.';
        $ticket=null; //Clear ticket obj.
    }
}
//At this stage we know the access status. we can process the post.
if($_POST && !$errors):

    if($ticket && $ticket->getId()) {
        //More coffee please.
        $errors=array();
        $lock=$ticket->getLock(); //Ticket lock if any
        $statusKeys=array('open'=>'Open','Reopen'=>'Open','Close'=>'Closed');
        switch(strtolower($_POST['a'])):
        case 'reply':
            if(!$thisstaff->canPostReply())
                $errors['err'] = 'Acesso negado. Contate a administração.';
            else {

                if(!$_POST['response'])
                    $errors['response']='A resposta não pode ser vazia!';
                //Use locks to avoid double replies
                if($lock && $lock->getStaffId()!=$thisstaff->getId())
                    $errors['err']='Ação negada. O tíquete está sendo editado.';

                //Make sure the email is not banned
                if(!$errors['err'] && TicketFilter::isBanned($ticket->getEmail()))
                    $errors['err']='O email de contato está banido. É necessário retirá-lo da lista de banidos para responder.';
            }

            $wasOpen =($ticket->isOpen());

            //If no error...do the do.
            $vars = $_POST;
            if(!$errors && $_FILES['attachments'])
                $vars['files'] = AttachmentFile::format($_FILES['attachments']);

            if(!$errors && ($response=$ticket->postReply($vars, $errors, isset($_POST['emailreply'])))) {
                $msg='Resposta postada com sucesso.';
                $ticket->reload();
                if($ticket->isClosed() && $wasOpen)
                    $ticket=null;

            } elseif(!$errors['err']) {
                $errors['err']='Não foi possível postar a resposta. Corrija os erros a seguir e tente novamente!';
            }
            break;
        case 'transfer': /** Transfer ticket **/
            //Check permission
            if(!$thisstaff->canTransferTickets())
                $errors['err']=$errors['transfer'] = 'Ação negada. Você não tem permissão para transferir tíquetes.';
            else {

                //Check target dept.
                if(!$_POST['deptId'])
                    $errors['deptId'] = 'Selecione um departamento';
                elseif($_POST['deptId']==$ticket->getDeptId())
                    $errors['deptId'] = 'O tíquete já está alocado para esse departamento.';
                elseif(!($dept=Dept::lookup($_POST['deptId'])))
                    $errors['deptId'] = 'Departamento desconhecido ou inválido.';

                //Transfer message - required.
                if(!$_POST['transfer_comments'])
                    $errors['transfer_comments'] = 'Adicione uma explicação para a transferência.';
                elseif(strlen($_POST['transfer_comments'])<5)
                    $errors['transfer_comments'] = 'A explicação está curta demais.';

                //If no errors - them attempt the transfer.
                if(!$errors && $ticket->transfer($_POST['deptId'], $_POST['transfer_comments'])) {
                    $msg = 'Tíquete transferido com sucesso para '.$ticket->getDeptName();
                    //Check to make sure the staff still has access to the ticket
                    if(!$ticket->checkStaffAccess($thisstaff))
                        $ticket=null;

                } elseif(!$errors['transfer']) {
                    $errors['err'] = 'Não foi possível completar a transferência do tíquete.';
                    $errors['transfer']='Corrija os erros abaixo e tente novamente!';
                }
            }
            break;
        case 'assign':

             if(!$thisstaff->canAssignTickets())
                 $errors['err']=$errors['assign'] = 'Acesso Negado. Você não tem permissão para alocar/desalocar tíquetes.';
             else {

                 $id = preg_replace("/[^0-9]/", "",$_POST['assignId']);
                 $claim = (is_numeric($_POST['assignId']) && $_POST['assignId']==$thisstaff->getId());

                 if(!$_POST['assignId'] || !$id)
                     $errors['assignId'] = 'Selecione para quem o tíquete será alocado';
                 elseif($_POST['assignId'][0]!='s' && $_POST['assignId'][0]!='t' && !$claim)
                     $errors['assignId']='Identificação inválida.';
                 elseif($ticket->isAssigned()) {
                     if($_POST['assignId'][0]=='s' && $id==$ticket->getStaffId())
                         $errors['assignId']='O tíquete já foi alocado para esse membro.';
                     elseif($_POST['assignId'][0]=='t' && $id==$ticket->getTeamId())
                         $errors['assignId']='O tíquete já foi alocado para essa equipe.';
                 }

                 //Comments are not required on self-assignment (claim)
                 if($claim && !$_POST['assign_comments'])
                     $_POST['assign_comments'] = 'Tíquete assumido por '.$thisstaff->getName();
                 elseif(!$_POST['assign_comments'])
                     $errors['assign_comments'] = 'É necessário comentário para realocação.';
                 elseif(strlen($_POST['assign_comments'])<5)
                         $errors['assign_comments'] = 'Comentário curto demais.';

                 if(!$errors && $ticket->assign($_POST['assignId'], $_POST['assign_comments'], !$claim)) {
                     if($claim) {
                         $msg = 'O tíquete agora é seu.';
                     } else {
                         $msg='Tíquete designado para '.$ticket->getAssigned();
                         TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());
                         $ticket=null;
                     }
                 } elseif(!$errors['assign']) {
                     $errors['err'] = 'Não foi possível completar a designação de tíquete.';
                     $errors['assign'] = 'Corrija os erros a seguir e tente novamente.';
                 }
             }
            break;
        case 'postnote': /* Post Internal Note */
            //Make sure the staff can set desired state
            if($_POST['state']) {
                if($_POST['state']=='closed' && !$thisstaff->canCloseTickets())
                    $errors['state'] = "Você não tem permissão para encerrar tíquetes.";
                elseif(in_array($_POST['state'], array('overdue', 'notdue', 'unassigned'))
                        && (!($dept=$ticket->getDept()) || !$dept->isManager($thisstaff)))
                    $errors['state'] = "Você não tem permissão para mudar o estado de um tíquete.";
            }

            $wasOpen = ($ticket->isOpen());

            $vars = $_POST;
            if($_FILES['attachments'])
                $vars['files'] = AttachmentFile::format($_FILES['attachments']);

            if(($note=$ticket->postNote($vars, $errors, $thisstaff))) {

                $msg='Nota interna postada com sucesso!';
                if($wasOpen && $ticket->isClosed())
                    $ticket = null; //Going back to main listing.

            } else {

                if(!$errors['err'])
                    $errors['err'] = 'Não foi possível postar a nota interna.';

                $errors['postnote'] = 'Não foi possível postar a nota interna. Corrija os erros abaixo e tente novamente.';
            }
            break;
        case 'edit':
        case 'update':
            if(!$ticket || !$thisstaff->canEditTickets())
                $errors['err']='Você não tem permissão para editar tíquetes.';
            elseif($ticket->update($_POST,$errors)) {
                $msg='Tíquete atualizado com sucesso.';
                $_REQUEST['a'] = null; //Clear edit action - going back to view.
                //Check to make sure the staff STILL has access post-update (e.g dept change).
                if(!$ticket->checkStaffAccess($thisstaff))
                    $ticket=null;
            } elseif(!$errors['err']) {
                $errors['err']='Não foi possível atualizar o tíquete. Corrija os erros abaixo e tente novamente.';
            }
            break;
        case 'process':
            switch(strtolower($_POST['do'])):
                case 'close':
                    if(!$thisstaff->canCloseTickets()) {
                        $errors['err'] = 'Você não tem permissão para encerrar tíquetes.';
                    } elseif($ticket->isClosed()) {
                        $errors['err'] = 'O tíquete já foi encerrado.';
                    } elseif($ticket->close()) {
                        $msg='O tíquete '.$ticket->getExtId().' foi encerrado.';
                        //Log internal note
                        if($_POST['ticket_status_notes'])
                            $note = $_POST['ticket_status_notes'];
                        else
                            $note='Tíquete encerrado sem comentários.';

                        $ticket->logNote('Tíquete encerrado.', $note, $thisstaff);

                        //Going back to main listing.
                        TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());
                        $page=$ticket=null;

                    } else {
                        $errors['err']='Ocorreu um erro ao encerrar o tíquete. Tente novamente.';
                    }
                    break;
                case 'reopen':
                    //if staff can close or create tickets ...then assume they can reopen.
                    if(!$thisstaff->canCloseTickets() && !$thisstaff->canCreateTickets()) {
                        $errors['err']='Você não tem permissão para reabrir tíquetes.';
                    } elseif($ticket->isOpen()) {
                        $errors['err'] = 'O tíquete já está aberto.';
                    } elseif($ticket->reopen()) {
                        $msg='Tíquete reaberto.';

                        if($_POST['ticket_status_notes'])
                            $note = $_POST['ticket_status_notes'];
                        else
                            $note='Tíquete reaberto sem comentários.';

                        $ticket->logNote('Ticket Reopened', $note, $thisstaff);

                    } else {
                        $errors['err']='Ocorreu um erro ao reabrir o tíquete, tente novamente.';
                    }
                    break;
                case 'release':
                    if(!$ticket->isAssigned() || !($assigned=$ticket->getAssigned())) {
                        $errors['err'] = 'O tíquete não está alocado!';
                    } elseif($ticket->release()) {
                        $msg='O tíquete não está mais alocado para '.$assigned;
                        $ticket->logActivity('Tíquete desalocado',$msg.' por '.$thisstaff->getName());
                    } else {
                        $errors['err'] = 'Ocorreu um erro ao desalocar o tíquete. Tente novamente.';
                    }
                    break;
                case 'claim':
                    if(!$thisstaff->canAssignTickets()) {
                        $errors['err'] = 'Você não tem permissão para alocar/assumir tíquetes.';
                    } elseif(!$ticket->isOpen()) {
                        $errors['err'] = 'Apenas tíquetes abertos podem ser alocados.';
                    } elseif($ticket->isAssigned()) {
                        $errors['err'] = 'O tíquete já foi alocado para '.$ticket->getAssigned();
                    } elseif($ticket->assignToStaff($thisstaff->getId(), ('Ticket claimed by '.$thisstaff->getName()), false)) {
                        $msg = 'Você assumiu o tíquete com sucesso.';
                    } else {
                        $errors['err'] = 'Ocorreu um erro ao designar o tíquete. Tente novamente.';
                    }
                    break;
                case 'overdue':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']='Você não tem permissão para marcar tíquetes como atrasados.';
                    } elseif($ticket->markOverdue()) {
                        $msg='Tíquete marcado como atrasado.';
                        $ticket->logActivity('Tíquete marcado como atrasado',($msg.' por '.$thisstaff->getName()));
                    } else {
                        $errors['err']='Ocorreu um erro ao marcar o tíquete como atrasado. Tente novamente.';
                    }
                    break;
                case 'answered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']='Você não tem permissão para marcar tíquetes.';
                    } elseif($ticket->markAnswered()) {
                        $msg='Tíquete marcado como respondido.';
                        $ticket->logActivity('Tíquete marcado como respondido',($msg.' por '.$thisstaff->getName()));
                    } else {
                        $errors['err']='Ocorreu um erro ao marcar o tíquete como respondido. Tente novamente.';
                    }
                    break;
                case 'unanswered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']='Você não tem permissão para marcar tíquetes.';
                    } elseif($ticket->markUnAnswered()) {
                        $msg='Tíquete marcado como não respondido.';
                        $ticket->logActivity('Tíquete marcado como não respondido',($msg.' por '.$thisstaff->getName()));
                    } else {
                        $errors['err']='Ocorreu um erro marcando o tíquete como não respondido. Tente novamente.';
                    }
                    break;
                case 'banemail':
                    if(!$thisstaff->canBanEmails()) {
                        $errors['err']='Você não tem permissão para banir emails.';
                    } elseif(BanList::includes($ticket->getEmail())) {
                        $errors['err']='Esse email já está banido.';
                    } elseif(Banlist::add($ticket->getEmail(),$thisstaff->getName())) {
                        $msg='Email ('.$ticket->getEmail().') banido.';
                    } else {
                        $errors['err']='Não foi possível adicionar o email à lista de emails banidos.';
                    }
                    break;
                case 'unbanemail':
                    if(!$thisstaff->canBanEmails()) {
                        $errors['err'] = 'Você não tem permissão para remover emails da lista de banidos.';
                    } elseif(Banlist::remove($ticket->getEmail())) {
                        $msg = 'Esse email não está mais banido.';
                    } elseif(!BanList::includes($ticket->getEmail())) {
                        $warn = 'esse email já não estava banido.';
                    } else {
                        $errors['err']='Não foi possível remover o email da lista de banidos. Tente novamente.';
                    }
                    break;
                case 'delete': // Dude what are you trying to hide? bad customer support??
                    if(!$thisstaff->canDeleteTickets()) {
                        $errors['err']='Você não tem permissão para apagar tíquetes.';
                    } elseif($ticket->delete()) {
                        $msg='Tíquete '.$ticket->getNumber().' apagado com sucesso.';
                        //Log a debug note
                        $ost->logDebug('Tíquete '.$ticket->getNumber().' apagado.',
                                sprintf('Tíquete #%s apagado por %s',
                                    $ticket->getNumber(), $thisstaff->getName())
                                );
                        $ticket=null; //clear the object.
                    } else {
                        $errors['err']='Erro ao apagar o tíquete. Tente novamente.';
                    }
                    break;
                default:
                    $errors['err']='Selecione um comando a executar.';
            endswitch;
            break;
        default:
            $errors['err']='Comando desconhecido.';
        endswitch;
        if($ticket && is_object($ticket))
            $ticket->reload();//Reload ticket info following post processing
    }elseif($_POST['a']) {

        switch($_POST['a']) {
            case 'mass_process':
                if(!$thisstaff->canManageTickets())
                    $errors['err']='You do not have permission to mass manage tickets. Contact admin for such access';
                elseif(!$_POST['tids'] || !is_array($_POST['tids']))
                    $errors['err']='No tickets selected. You must select at least one ticket.';
                else {
                    $count=count($_POST['tids']);
                    $i = 0;
                    switch(strtolower($_POST['do'])) {
                        case 'reopen':
                            if($thisstaff->canCloseTickets() || $thisstaff->canCreateTickets()) {
                                $note='Tíquete reaberto por '.$thisstaff->getName();
                                foreach($_POST['tids'] as $k=>$v) {
                                    if(($t=Ticket::lookup($v)) && $t->isClosed() && @$t->reopen()) {
                                        $i++;
                                        $t->logNote('Tiquete reaberto', $note, $thisstaff);
                                    }
                                }

                                if($i==$count)
                                    $msg = "Tíquetes selecionados ($i) reabertos com sucesso.";
                                elseif($i)
                                    $warn = "$i de $count tíquetes selecionados foram reabertos.";
                                else
                                    $errors['err'] = 'Erro ao reabrir tíquetes.';
                            } else {
                                $errors['err'] = 'Você não tem permissão para reabrir tíquetes.';
                            }
                            break;
                        case 'close':
                            if($thisstaff->canCloseTickets()) {
                                $note='Tíquete fechado sem resposta por '.$thisstaff->getName();
                                foreach($_POST['tids'] as $k=>$v) {
                                    if(($t=Ticket::lookup($v)) && $t->isOpen() && @$t->close()) {
                                        $i++;
                                        $t->logNote('Tíquete fechado', $note, $thisstaff);
                                    }
                                }

                                if($i==$count)
                                    $msg ="Tíquetes selecionados ($i) fechados com sucesso.";
                                elseif($i)
                                    $warn = "$i de $count tíquetes foram fechados.";
                                else
                                    $errors['err'] = 'Unable to close selected tickets';
                            } else {
                                $errors['err'] = 'Você não tem permissão para fechar tíquetes.';
                            }
                            break;
                        case 'mark_overdue':
                            $note='O tíquete foi marcado como atrasado por '.$thisstaff->getName();
                            foreach($_POST['tids'] as $k=>$v) {
                                if(($t=Ticket::lookup($v)) && !$t->isOverdue() && $t->markOverdue()) {
                                    $i++;
                                    $t->logNote('Tíquete marcado como atrasado', $note, $thisstaff);
                                }
                            }

                            if($i==$count)
                                $msg = "Tíquetes selecionados ($i) marcados como atrasados.";
                            elseif($i)
                                $warn = "$i de $count tíquetes foram marcados como atrasados";
                            else
                                $errors['err'] = 'Não foi possível marcar os tíquetes como atrasados.';
                            break;
                        case 'delete':
                            if($thisstaff->canDeleteTickets()) {
                                foreach($_POST['tids'] as $k=>$v) {
                                    if(($t=Ticket::lookup($v)) && @$t->delete()) $i++;
                                }

                                //Log a warning
                                if($i) {
                                    $log = sprintf('%s (%s) apagou %d tíquete(s)',
                                            $thisstaff->getName(), $thisstaff->getUserName(), $i);
                                    $ost->logWarning('Tíquetes apagados', $log, false);

                                }

                                if($i==$count)
                                    $msg = "Tíquetes selecionados ($i) apagados com sucesso.";
                                elseif($i)
                                    $warn = "$i de $count tíquetes foram apagados.";
                                else
                                    $errors['err'] = 'Não foi possível apagar os tíquetes.';
                            } else {
                                $errors['err'] = 'Você não tem permissão para deletar tíquetes.';
                            }
                            break;
                        default:
                            $errors['err']='Ação desconhecida.';
                    }
                }
                break;
            case 'open':
                $ticket=null;
                if(!$thisstaff || !$thisstaff->canCreateTickets()) {
                     $errors['err']='Você não tem permissão para abrir tíquetes.';
                } else {
                    $vars = $_POST;
                    if($_FILES['attachments'])
                        $vars['files'] = AttachmentFile::format($_FILES['attachments']);

                    if(($ticket=Ticket::open($vars, $errors))) {
                        $msg='Tíquete criado com sucesso.';
                        $_REQUEST['a']=null;
                        if(!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                    } elseif(!$errors['err']) {
                        $errors['err']='Não foi possível criar o tíquete. Tente novamente.';
                    }
                }
                break;
        }
    }
    if(!$errors)
        $thisstaff ->resetStats(); //We'll need to reflect any changes just made!
endif;

/*... Quick stats ...*/
$stats= $thisstaff->getTicketsStats();

//Navigation
$nav->setTabActive('tickets');
if($cfg->showAnsweredTickets()) {
    $nav->addSubMenu(array('desc'=>'Open ('.number_format($stats['open']+$stats['answered']).')',
                            'title'=>'Open Tickets',
                            'href'=>'tickets.php',
                            'iconclass'=>'Ticket'),
                        (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
} else {

    if($stats) {
        $nav->addSubMenu(array('desc'=>'Open ('.number_format($stats['open']).')',
                               'title'=>'Open Tickets',
                               'href'=>'tickets.php',
                               'iconclass'=>'Ticket'),
                            (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
    }

    if($stats['answered']) {
        $nav->addSubMenu(array('desc'=>'Answered ('.number_format($stats['answered']).')',
                               'title'=>'Answered Tickets',
                               'href'=>'tickets.php?status=answered',
                               'iconclass'=>'answeredTickets'),
                            ($_REQUEST['status']=='answered'));
    }
}

if($stats['assigned']) {
    if(!$ost->getWarning() && $stats['assigned']>10)
        $ost->setWarning($stats['assigned'].' tíquetes estão alocados para você.');

    $nav->addSubMenu(array('desc'=>'My Tickets ('.number_format($stats['assigned']).')',
                           'title'=>'Assigned Tickets',
                           'href'=>'tickets.php?status=assigned',
                           'iconclass'=>'assignedTickets'),
                        ($_REQUEST['status']=='assigned'));
}

if($stats['overdue']) {
    $nav->addSubMenu(array('desc'=>'Overdue ('.number_format($stats['overdue']).')',
                           'title'=>'Stale Tickets',
                           'href'=>'tickets.php?status=overdue',
                           'iconclass'=>'overdueTickets'),
                        ($_REQUEST['status']=='overdue'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=$stats['overdue'] .' overdue tickets!';
}

if($thisstaff->showAssignedOnly() && $stats['closed']) {
    $nav->addSubMenu(array('desc'=>'My Closed Tickets ('.number_format($stats['closed']).')',
                           'title'=>'My Closed Tickets',
                           'href'=>'tickets.php?status=closed',
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
} else {

    $nav->addSubMenu(array('desc'=>'Closed Tickets ('.number_format($stats['closed']).')',
                           'title'=>'Closed Tickets',
                           'href'=>'tickets.php?status=closed',
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
}

if($thisstaff->canCreateTickets()) {
    $nav->addSubMenu(array('desc'=>'New Ticket',
                           'href'=>'tickets.php?a=open',
                           'iconclass'=>'newTicket'),
                        ($_REQUEST['a']=='open'));
}


$inc = 'tickets.inc.php';
if($ticket) {
    $ost->setPageTitle('Ticket #'.$ticket->getNumber());
    $nav->setActiveSubMenu(-1);
    $inc = 'ticket-view.inc.php';
    if($_REQUEST['a']=='edit' && $thisstaff->canEditTickets())
        $inc = 'ticket-edit.inc.php';
    elseif($_REQUEST['a'] == 'print' && !$ticket->pdfExport($_REQUEST['psize'], $_REQUEST['notes']))
        $errors['err'] = 'Erro interno: não foi possível exportar o tíquete para formato PDF para impressão.';
} else {
    $inc = 'tickets.inc.php';
    if($_REQUEST['a']=='open' && $thisstaff->canCreateTickets())
        $inc = 'ticket-open.inc.php';
    elseif($_REQUEST['a'] == 'export') {
        require_once(INCLUDE_DIR.'class.export.php');
        $ts = strftime('%Y%m%d');
        if (!($token=$_REQUEST['h']))
            $errors['err'] = 'Query token required';
        elseif (!($query=$_SESSION['search_'.$token]))
            $errors['err'] = 'Query token not found';
        elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv'))
            $errors['err'] = 'Internal error: Unable to dump query results';
    }

    //Clear active submenu on search with no status
    if($_REQUEST['a']=='search' && !$_REQUEST['status'])
        $nav->setActiveSubMenu(-1);

    //set refresh rate if the user has it configured
    if(!$_POST && !$_REQUEST['a'] && ($min=$thisstaff->getRefreshRate()))
        $ost->addExtraHeader('<meta http-equiv="refresh" content="'.($min*60).'" />');
}

require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.$inc);
require_once(STAFFINC_DIR.'footer.inc.php');
?>
