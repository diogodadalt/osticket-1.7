<?php
/*********************************************************************
    templates.php

    Email Templates

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
include_once(INCLUDE_DIR.'class.template.php');
$template=null;
if($_REQUEST['tpl_id'] &&
        !($template=EmailTemplateGroup::lookup($_REQUEST['tpl_id'])))
    $errors['err']='Template de identificação de grupo desconhecido ou inválido.';
elseif($_REQUEST['id'] &&
        !($template=EmailTemplate::lookup($_REQUEST['id'])))
    $errors['err']='Template de identificação desconhecido ou inválido.';

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'updatetpl':
            if(!$template){
                $errors['err']='Template desconhecido ou inválido.';
            }elseif($template->update($_POST,$errors)){
                $template->reload();
                $msg='Template de mensagem postado com sucesso.';
            }elseif(!$errors['err']){
                $errors['err']='Erro ao atualizar o template. Tente Novamente.';
            }
            break;
        case 'implement':
            if(!$template){
                $errors['err']='Template desconhecido ou inválido.';
            }elseif($new = EmailTemplate::add($_POST,$errors)){
                $template = $new;
                $msg='Template de mensagem atualizado com sucesso.';
            }elseif(!$errors['err']){
                $errors['err']='Erro ao atualizar o template. Tente Novamente.';
            }
            break;
        case 'update':
            if(!$template){
                $errors['err']='Template desconhecido ou inválido.';
            }elseif($template->update($_POST,$errors)){
                $msg='Template atualizado com sucesso.';
            }elseif(!$errors['err']){
                $errors['err']='Erro ao atualizar o template. Tente Novamente.';
            }
            break;
        case 'add':
            if(($new=EmailTemplateGroup::add($_POST,$errors))){
                $template=$new;
                $msg='Template adicionado com sucesso.';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Não foi possível adicionar o template. Corrija os erros abaixo e tente novamente.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err']='Selecione um template para continuar.';
            } else {
                $count=count($_POST['ids']);
                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE '.EMAIL_TEMPLATE_GRP_TABLE.' SET isactive=1 '
                            .' WHERE tpl_id IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())){
                            if($num==$count)
                                $msg = 'Os templates selecionados foram ativados.';
                            else
                                $warn = "$num de $count templates foram ativados.";
                        } else {
                            $errors['err'] = 'Não foi possível ativar os templates. Tente novamente.';
                        }
                        break;
                    case 'disable':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($t=EmailTemplateGroup::lookup($v)) && !$t->isInUse() && $t->disable())
                                $i++;
                        }
                        if($i && $i==$count)
                            $msg = 'Os templates selecionados foram desativados.';
                        elseif($i)
                            $warn = "$i de $count templates foram desativados. Lembre-se que templates em uso não podem ser desativados.";
                        else
                            $errors['err'] = "Não foi possível desativar os templates. Lembre-se que templates em uso não podem ser desativados.";
                        break;
                    case 'delete':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($t=EmailTemplateGroup::lookup($v)) && !$t->isInUse() && $t->delete())
                                $i++;
                        }

                        if($i && $i==$count)
                            $msg = 'Os templates selecionados foram apagados.';
                        elseif($i>0)
                            $warn = "$i de $count templates foram apagados.";
                        elseif(!$errors['err'])
                            $errors['err'] = 'Não foi possível apagar os templates.';
                        break;
                    default:
                        $errors['err']='Ação desconhecida.';
                }
            }
            break;
        default:
            $errors['err']='Ação desconhecida.';
            break;
    }
}

$page='templates.inc.php';
if($template && !strcasecmp($_REQUEST['a'],'manage')){
    $page='tpl.inc.php';
}elseif($template && !strcasecmp($_REQUEST['a'],'implement')){
    $page='tpl.inc.php';
}elseif($template || !strcasecmp($_REQUEST['a'],'add')){
    $page='template.inc.php';
}

$nav->setTabActive('emails');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
