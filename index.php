<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('client.inc.php');
$section = 'home';
require(CLIENTINC_DIR.'header.inc.php');
?>
<div id="landing_page">
    <?php
    if($cfg && ($page = $cfg->getLandingPage()))
        echo $page->getBody();
    else
        echo  '<h1>Bem-vindo ao centro de suporte do Instituto de Informática.</h1>';
    ?>
    <div id="new_ticket">
        <h3>Nova requisição de suporte</h3>
        <br>
        <div>Por favor, forneça detalhes sobre o problema para agilizar o atendimento. Para atualizar um tíquete aberto anteriormente, identifique-se.</div>
        <p>
            <a href="open.php" class="green button">Open a New Ticket</a>
        </p>
    </div>

    <div id="check_status">
        <h3>Checar Requisição</h3>
        <br>
        <div>Nós providenciamos arquivos com todas as suas requisições de suporte, com respostas.<div>
        <p>
            <a href="view.php" class="blue button">Check Ticket Status</a>
        </p>
    </div>
</div>
<div class="clear"></div>
<?php
if($cfg && $cfg->isKnowledgebaseEnabled()){
    //FIXME: provide ability to feature or select random FAQs ??
?>
<p>Cheque a seção de <a href="kb/index.php">Perguntas Frequentes</a>, antes de abrir uma requisição.</p>
</div>
<?php
} ?>
<?php require(CLIENTINC_DIR.'footer.inc.php'); ?>
